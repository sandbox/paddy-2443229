<?php

/**
 * @file
 * Contains the custom fetcher code for Feeds Evernote
 */

/**
 * Definition of EvernoteFetcher.
 */
class EvernoteCloudFetcherResult extends FeedsFetcherResult {
  protected $config;

  /**
   * Constructor.
   */
  public function __construct($config) {
    $this->config = $config;
    parent::__construct('');
  }

  /**
   * Implements getRaw().
   */
  public function getRaw() {
    feeds_evernote_cloud_includes();

    $client = feeds_evernote_cloud_authenticate($this->config);
    $token = $client->getToken();
    if (!$client) {
      return array();
    }
    try {
      $note_store = $client->getUserNotestore();
      if (FALSE) {
        $note_store = new \EDAM\NoteStore\NoteStoreClient(NULL);
      }

      $filter = new \EDAM\NoteStore\NoteFilter();
      if ($this->config['notebook_guid'] != '0') {
        $filter->notebookGuid = $this->config['notebook_guid'];
      }
      if ($this->config['search_guid'] != '0') {
        $search = $note_store->getSearch($token, $this->config['search_guid']);
        if (FALSE) {
          $search = new \EDAM\Types\SavedSearch();
        }
        $filter->words = $search->query;
      }
      if ($this->config['tag_guid'] != '0') {
        $filter->tagGuids = array($this->config['tag_guid']);
      }

      // Fetch notes based on filters and authorisation.
      $spec = new \EDAM\NoteStore\NotesMetadataResultSpec();
      $spec->includeTitle = TRUE;
      $spec->includeContentLength = TRUE;
      $spec->includeCreated = TRUE;
      $spec->includeUpdated = TRUE;
      $spec->includeUpdateSequenceNum = TRUE;
      $spec->includeNotebookGuid = TRUE;
      $spec->includeTagGuids = TRUE;
      $spec->includeAttributes = TRUE;
      $spec->includeLargestResourceMime = TRUE;
      $spec->includeLargestResourceSize = TRUE;

      $offset = 0;
      $result = array();
      do {
        $notes_list = $note_store->findNotesMetadata($token, $filter, $offset, 20, $spec);
        $note_store->findNotesMetadata($token, $filter, $offset, 20, $spec);
        if (FALSE) {
          $notes_list = new \EDAM\NoteStore\NoteList();
        }
        $offset = $notes_list->startIndex + count($notes_list->notes);
        $remain = $notes_list->totalNotes - $offset;
        foreach ($notes_list->notes as $note) {
          $result[] = $note;
        }
      } while ($remain > 0);

      // Array will be used by EvernoteCloudParser.
      return array(
        'token' => $token,
        'notes' => $result,
        'config' => $this->config,
        'note_store' => $note_store,
      );
    }
    catch (\EDAM\Error\EDAMUserException $e) {
      watchdog('Feeds Evernote Cloud', __FUNCTION__ . ': Error getting note store: ' . $e->getMessage(), NULL, WATCHDOG_WARNING, NULL);
      return array();
    }
  }
}

class EvernoteCloudFetcher extends FeedsFetcher {

  /**
   * Implementation of FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    $config = array_merge($this->config, $source->getConfigFor($this));
    return new EvernoteCloudFetcherResult($config);
  }

  /**
   * Implementation of FeedsFetcher::sourceForm().
   */
  public function sourceForm($source_config) {
    $config = array_merge($this->config, $source_config);
    $form = $this->settingsForm($config, 'sourceForm');
    $form['blank'] = array(
      '#type' => 'hidden',
      '#default_value' => 'blank',
    );
    // Source form requires at least one form element.
    return $form;
  }

  /**
   * Implementation of FeedsFetcher::sourceFormValidate().
   */
  public function sourceFormValidate(&$values) {
    $values = array_merge($this->config, $values);
    // These should never be saved by the source form.
    $values['filter_allow_override'] = $values['auth_allow_override'] = '';
    $this->settingsValidate($values);
  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
      'consumer_key' => '',
      'consumer_secret' => '',
      'evernote_server' => 'sandbox',
      'sandbox' => TRUE,
      'reset' => FALSE,
      'notebook_name' => '',
      'notebook_guid' => '',
      'search_name' => '',
      'search_guid' => '',
      'tag_name' => '',
      'tag_guid' => '',
      'auth_allow_override' => '',
      'filter_allow_override' => '',
    );
  }

  /**
   * Config form.
   *
   * @return array
   *   Configuration form settings
   */
  public function configForm(&$form_state) {
    return $this->settingsForm($this->config, 'configForm');
  }

  /**
   * Config form.
   */
  public function configFormValidate(&$values) {
    $this->settingsValidate($values);
  }

  /**
   * Configuration form default settings.
   *
   * @param array $config
   *   Form configuration settings.
   * @param string $form_name
   *   Form name.
   *
   * @return array
   *   Form configuration settings.
   */
  protected function settingsForm(&$config, $form_name) {
    $form = array();
    $filter_notes = array(0 => 'All note books');
    $filter_search = array(0 => 'No search filters');
    $filter_tags = array(0 => 'All tags');

    if (!is_null(variable_get('feeds_evernote_cloud_access_token', NULL))) {
      $client = feeds_evernote_cloud_authenticate();
      $token = $client->getToken();

      if ($client) {
        $note_store = feeds_evernote_cloud_note_store($client);
        if (FALSE) {
          $note_store = new \EDAM\NoteStore\NoteStoreClient(NULL);
        }
        $notes = $note_store->listNotebooks($token);
        $filtr = $note_store->listSearches($token);
        $tags  = $note_store->listTags($token);

        foreach ($notes as $n) {
          $filter_notes[$n->guid] = $n->name;
        }
        foreach ($filtr as $f) {
          $filter_search[$f->guid] = $f->name;
        }
        foreach ($tags as $t) {
          $filter_tags[$t->guid] = $t->name;
        }
      }
    }

    if ('configForm' == $form_name || $config['auth_allow_override']) {
      $form['oauth'] = array(
        '#type' => 'fieldset',
        '#title' => t('Evernote Authentication'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
      $form['oauth']['consumer_key'] = array(
        '#prefix' => '<p>Please be sure to request a separate API key for each application that you develop.</p><p>First, request an Evernote API Key at https://dev.evernote.com/#apikey.</p>',
        '#type' => 'textfield',
        '#title' => t('Consumer Key'),
        '#default_value' => variable_get('feeds_evernote_cloud_consumer_key'),
        '#disabled' => !is_null(variable_get('feeds_evernote_cloud_access_token', NULL)),
      );
      $form['oauth']['consumer_secret'] = array(
        '#type' => 'textfield',
        '#title' => t('Consumer Secret'),
        '#default_value' => variable_get('feeds_evernote_cloud_consumer_secret'),
        '#disabled' => !is_null(variable_get('feeds_evernote_cloud_access_token', NULL)),
      );
      $form['oauth']['evernote_server'] = array(
        '#type' => 'select',
        '#title' => t('Evernote server'),
        '#default_value' => $config['evernote_server'],
        '#options' => array(
          'https://sandbox.evernote.com/' => 'Sandbox',
          'https://www.evernote.com/' => 'Production (International)',
          'https://app.yinxiang.com/' => 'Production (China)',
        ),
        '#description' => t('https://sandbox.evernote.com/ or https://www.evernote.com/, depending on the target Evernote service - sandbox or production.'),
        '#disabled' => !is_null(variable_get('feeds_evernote_cloud_access_token', NULL)),
      );
      $form['oauth']['reset'] = array(
        '#type' => 'checkbox',
        '#title' => t('Reset Evernote credentials'),
        '#default_value' => 0,
        '#description' => t('Reset Evernote OAuth credentials.'),
        '#disabled' => is_null(variable_get('feeds_evernote_cloud_access_token', NULL)),
      );

      if ('configForm' == $form_name) {
        $form['oauth']['auth_allow_override'] = array(
          '#type' => 'checkbox',
          '#title' => t('Allow authentication settings to be overridden on source form'),
          '#default_value' => $config['auth_allow_override'],
        );
      }
    }

    if ('configForm' == $form_name || $config['filter_allow_override']) {
      $form['filters'] = array(
        '#type' => 'fieldset',
        '#title' => t('Evernote Filters'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
      $form['filters']['notebook_guid'] = array(
        '#type' => 'select',
        '#title' => t('Notebook name'),
        '#options' => $filter_notes,
        '#default_value' => $config['notebook_guid'],
        '#description' => t('Filter the notes that will be imported to a specific Evernote notebook'),
      );

      $form['filters']['search_guid'] = array(
        '#type' => 'select',
        '#title' => t('Evernote search'),
        '#default_value' => $config['search_guid'],
        '#options' => $filter_search,
        '#description' => t('Filter the notes that will be imported to a specific Evernote saved search'),
      );

      $form['filters']['tag_guid'] = array(
        '#type' => 'select',
        '#title' => t('Evernote tag'),
        '#default_value' => $config['tag_guid'],
        '#options' => $filter_tags,
        '#description' => t('Filter the notes that will be imported to a specific Evernote tag'),
      );

      if ('configForm' == $form_name) {
        $form['filters']['filter_allow_override'] = array(
          '#type' => 'checkbox',
          '#title' => t('Allow filter settings to be overridden on source form'),
          '#default_value' => $config['filter_allow_override'],
        );
      }
    }
    return $form;
  }

  /**
   * Validate form settings.
   *
   * @param array $values
   *   Configuration form settings
   */
  protected function settingsValidate(&$values) {
    if (isset($values['reset']) && $values['reset'] != 0) {
      variable_del('feeds_evernote_cloud_access_token');
    }
    else {
      if (!empty($values['consumer_key']) && !empty($values['consumer_secret']) && is_null(variable_get('feeds_evernote_cloud_access_token', NULL))) {
        variable_set('feeds_evernote_cloud_consumer_key', $values['consumer_key']);
        variable_set('feeds_evernote_cloud_consumer_secret', $values['consumer_secret']);
        $client = feeds_evernote_cloud_get_authorisation();
        if (!$client) {
          variable_del('feeds_evernote_cloud_access_token');
        }
      }
    }
    variable_set('feeds_evernote_cloud_server', $values['evernote_server']);
  }
}
