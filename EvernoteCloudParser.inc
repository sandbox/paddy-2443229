<?php
/**
 * @file
 * Evernote Parser.
 */

class EvernoteCloudParser extends FeedsParser {

  /**
   * Implements FeedsParser::parse().
   *
   * @param FeedsSource $source
   *   Feed source
   * @param FeedsFetcherResult $fetcher_result
   *   Results from running the fetcher
   *
   * @return object
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {
    feeds_evernote_cloud_includes();

    $result = new FeedsParserResult();
    $raw = $fetcher_result->getRaw();
    if (FALSE) {
      unset($source);
    }
    if (!empty($raw)) {
      $notes = $raw['notes'];
      $token = $raw['token'];
      foreach ($notes as $note) {
        if (FALSE) {
          $raw['note_store'] = new \EDAM\NoteStore\NoteStoreClient(NULL);
        }
        $note_info = $raw['note_store']->getNote($token, $note->guid, TRUE, TRUE, TRUE, TRUE);
        if (FALSE) {
          $note_info = new \EDAM\Types\Note();
          $note_info->attributes = new \EDAM\Types\NoteAttributes();
        }

        $tags = array();
        $guids = $note_info->tagGuids;
        if (!empty($guids)) {
          foreach ($guids as $tag_guid) {
            $tag = $raw['note_store']->getTag($token, $tag_guid);
            if (FALSE) {
              $tag = new \EDAM\Types\Tag();
            }
            $tags[] = $tag->name;
          }
        }
        $files = feeds_evernote_cloud_get_files($note_info);

        $result->items[] = array(
          'title' => $note_info->title,
          'content' => feeds_evernote_cloud_sanitize_body($note_info->content, $raw['note_store'], $note_info->guid, $note_info->attributes),
          // Convert milliseconds to seconds.
          'created' => ($note_info->created / 1000),
          'guid' => $note_info->guid,
          'source_url' => $note_info->attributes->sourceURL,
          'updateSequenceNum' => $note_info->updateSequenceNum,
          'tags' => $tags,
          'image' => isset($files['image']) ? $files['image'] : array(),
          'files' => isset($files['file']) ? $files['file'] : array(),
        );
      }
    }
    return $result;
  }

  /**
   * Return mapping sources.
   *
   * At a future point, we could expose data type information here,
   * storage systems like Data module could use this information to store
   * parsed data automatically in fields with a correct field type.
   */
  public function getMappingSources() {
    return array(
      'title' => array(
        'name' => t('Title'),
        'description' => t('Title of the feed item.'),
      ),
      'content' => array(
        'name' => t('Content'),
        'description' => t('Content of the feed item.'),
      ),
      'created' => array(
        'name' => t('Published date'),
        'description' => t('Published date as UNIX time GMT of the feed item.'),
      ),
      'source_url' => array(
        'name' => t('Source URL'),
        'description' => t('Source URL of the feed item.'),
      ),
      'guid' => array(
        'name' => t('Item GUID'),
        'description' => t('Global Unique Identifier of the feed item.'),
      ),
      'tags' => array(
        'name' => t('Tags'),
        'description' => t('An array of categories that have been assigned to the feed item.'),
      ),
      'image' => array(
        'name' => t('Image'),
        'description' => t('Images'),
      ),
      'file' => array(
        'name' => t('File'),
        'description' => t('Files'),
      ),
    ) + parent::getMappingSources();
  }
}
