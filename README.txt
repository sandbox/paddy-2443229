CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

 INTRODUCTION
 ------------
 This Drupal Feeds importer for Evernote, allows you to connect - using OAUTH -
 your web site and your Evernote account.

 When configured, the Drupal Feeds importer for Evernote will connect to your
 Evernote account, search for content that should be published (notebook,
 saved search filter, or tag) and copy this content and publish it.

 REQUIREMENTS
 ------------
 This module requires the following modules:
  * Libraries (https://drupal.org/project/libraries)
  * Feeds (https://drupal.org/project/feeds)

 RECOMMENDED MODULES
 -------------------
 This module recommends the following modules:
  * none

 INSTALLATION
 ------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * Install Evernote Cloud SDK (https://github.com/evernote/evernote-cloud-sdk-php)
   - Download https://github.com/evernote/evernote-cloud-sdk-php/archive/master.zip
     . Expand ZIP file in the sites libraries folder (sites/all/libraries)
     . Ensure that the newly expanded folder is named 'evernote'
       . sites/all/libraries/evernote/README.md should exist
   - OauthHandler.php
     Several tests have shown that the PHP function header() used in OauthHandler.PHP
     does not
     . Update sites/all/libraries/evernote/src/Evernote/Auth/OauthHandler.php
       . Replace (circa line 54)
          $authorizationUrl = 'Location: '
            . $this->getBaseUrl('OAuth.action?oauth_token=')
            . $temporaryCredentials['oauth_token'];
         with
           if (defined(DRUPAL_ROOT)) {
             $authorizationUrl = $this->getBaseUrl('OAuth.action?oauth_token=')
             . $temporaryCredentials['oauth_token'];
           } else {
             $authorizationUrl = 'Location: '
             . $this->getBaseUrl('OAuth.action?oauth_token=')
             . $temporaryCredentials['oauth_token'];
           }
       . Replace (circa line 62)
           header($authorizationUrl);
         with
           if (defined(DRUPAL_ROOT)) {
             drupal_goto($authorizationUrl);
           } else {
             header($authorizationUrl);
           }

 CONFIGURATION
 -------------

 * Configure a new feed importer

   - Create new feed importer admin/structure/feeds/create

   - Basic settings
     . Attach to content type: Use standalone form
     . Periodic import: set to reasonable delay (according to your blogging activity)

   - Fetcher
     . Select Evernote Fetcher

     . Evernote Fetcher settings
       . Consumer Key: provided by Evernote (https://dev.evernote.com/#apikey)
       . Consumer Secret: provided by Evernote (https://dev.evernote.com/#apikey)
       . Evernote server: use Sandbox and then production
         NB: Full access is required to Evernote account

       . Notebook name: Notebook to use to publish to website
       . Evernote search: Saved search/filter to use to publish to website
       . Evernote tag: Saved tag to use to publish to website

   - Parser
     . Select Evernote Parser

     . Evernote Parser Settings
       . none

   Processor
     . Select Node processor

     . Node processor Settings
       . Bundle: Article / Page / Blog entry / ...
       . Update existing nodes: Do not update existing nodes
       . Skip hash check: Off
       . Text format: Filtered HTML / Full HTML / ...
       . Author: <your username>
       . Authorize: yes
       . Expire nodes: Never

     . Node processor Mappings
       . Title          -> Title
       . Content        -> Body
       . Published date -> Published date
       . Tags           -> Tags (local folksonomy)
       . Item GUID      -> GUID (*Used as unique*)
       . Source URL     -> ...
       . File           -> ...
       . Image          -> ...

 TROUBLESHOOTING
 ---------------
 * Verify that credentials works on Sandbox before trying on Production

 * Send request to Evernote to upgrade from development Sandbox to Production

 * Ensure that Evernote API credentials support Full access to Evernote content

 * Credentials need to be renewed every year

 FAQ
 ---

 MAINTAINERS
 -----------
 Current maintainers:
  * Paddy de Búrca (paddy_deburca) - https://www.drupal.org/user/15867